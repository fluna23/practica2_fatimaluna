using System;
namespace Ejercicio2
{
    class Motor {
        private int numeroCilindros;
        private string tipoCarburador;
        public string TipoCombustible {get; set;}
        public int obtenerNumeroCilindros(){
            return numeroCilindros;
        }
        public void asignarNumeroCilindros(int cantidad) {
            numeroCilindros = cantidad;
        }
        public string obtenerTipoCarburador(){
            return tipoCarburador;
        }
        public void asignarTipoCarburador(string tipoCarburador) {
            this.tipoCarburador = tipoCarburador;
        }
        public void consumirCombustible(ref Tanque tanque, decimal cantidadLitros) {
            if( tanque != null){
                tanque.vaciarTanque(cantidadLitros*(decimal)this.numeroCilindros);
            }
        }
    }
}